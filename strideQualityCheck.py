#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 09:50:46 2021

@author: cyrilb
"""

from helperFunctions import labelOutlierMetric

def strideTimeConsistency(df, timeRange):
    """ 
    A simple function that check if timestamp and clientTimestamp are inside a range
    INPUT: 
        df: a record imported as dataframe
        timeRange: a dictionnary containing the range {timestamp: [0, 1000], clienTimestamp: [0, 1000]}
    OUTPUT:
        the input df with additional columns containing Bool (True: the value is inside the range)
    
    """
    df['ts_inf'] = df.timestamp > timeRange['timestamp'][0]
    df['ts_sup'] = df.timestamp < timeRange['timestamp'][1]
    df['cTs_inf'] = df.clientTimestamp > timeRange['clientTimestamp'][0]      
    df['cTs_sup'] = df.clientTimestamp < timeRange['clientTimestamp'][1]
    
    return df
     
        
def strideInsideRecord(df, startTime):
    """ 
    Function that check of each time value of a stride (timeHeelStrik1, timeHeelStrike2,
    timeToeOff, clientTImestamp), is inside the startTime, stopTime of the record.
    
    INPUT: 
        df: a record imported as dataframe
        startTime: the startTime of the record (integer)
        stopTime: the stopTime of the record (integer)
    OUPUT:
        df: the same df with additional columns (True : the data is inside the range)
        
    LEGEND:
        cTs: clientTImestamp
        tHS1: timeHeelStrike1
        tHS2: timeHeelStrike2
        tTO: timeToeOff
        staT: startTime
        stoT: stopTime
    
    """
    # Check if the stride is inside the record

    df['cTs_staT'] = df.clientTimestamp < int(startTime)
    df['tHS1_staT'] = df.timeHeelStrike1 < int(startTime)    # not an error it's  implemented like that
    df['tTO_staT'] = df.timeToeOff < int(startTime)    # not an error it's  implemented like that
    df['tHS2_staT'] = df.timeHeelStrike2 < int(startTime)
    
    return df


def intraStrideSequenceConsistency(df):
    """ Function that check that the sequence of time events inside a stride is consistent:
            Time heel strike 1 < time toe off < Time heel strike 2 < clientTimestamp

    INPUT:
        df: a record imported as dataframe   
    OUTPUT:
        df: the same df with additional columns (True : the data is inside the range)
        
    LEGEND:
        cTs: clientTImestamp
        tHS1: timeHeelStrike1
        tHS2: timeHeelStrike2
        tTO: timeToeOff
    """
    # QC of the logical scheduling of the stride 'time' metrics
    # timestamp and cli
    try:
        # timestamp and clientTimestamp inside a define range

        # timeHeelStrike1 < timeToeOff < timeHeelStrike2 < clientTimestamp
        df['tHS1_tTO'] = df.timeHeelStrike1 < df.timeToeOff        
        df['tHS1_tHS2'] = df.timeHeelStrike1 < df.timeHeelStrike2  
        df['tTO_tTHS2'] = df.timeToeOff < df.timeHeelStrike2       
        df['tHS1_cTs'] = df.timeHeelStrike1 < df.clientTimestamp   
        df['tTO_cTs'] = df.timeToeOff < df.clientTimestamp             
        df['tHS2_cTs'] = df.timeHeelStrike2 < df.clientTimestamp   
        
    except:
        print('Error during time Quality Check')
        
    return df
        


def interStrideSequenceConsistency(df):
    """
    This function test by side if consecutive strides do not seems abberant.
    
    INPUT:
        df: a record imported as dataframe   
    OUTPUT:
        df: the same df with additional columns (True : the data is inside the range)
    """
    for a_side in ['left', 'right']:
        tmp = df.loc[df.side == a_side]
        df.loc[tmp.index, 'ts2_ts1'] = ((tmp.timestamp - tmp.timestamp.shift()).fillna(0) > 0)
        df.loc[tmp.index, 'cTs2_cTs1'] = ((tmp.clientTimestamp - tmp.clientTimestamp.shift()).fillna(0) > 0)
        df.cTs2_cTs1 = df.cTs2_cTs1.astype(bool)
        df.ts2_ts1 = df.ts2_ts1.astype(bool)

        
    return df


def nanCheck(df):
    """ 
    This function count the number of nan value per stride
    In addition it also created a col that say if their is no Nan in the stride
    
    INPUT:
        df: a record imported as dataframe   
    OUTPUT:
        df: the same df with additional columns
    
    """
    df['nbNA'] = df.isna().sum(axis=1)
    df['noNAN'] = df['nbNA'] == 0
    
    return df


def duplicatedStrideCheck(df):
    """ 
    This function label duplicated stride based on widthMotion, strideElevation and strideLength
    
    INPUT:
        df: a record imported as dataframe   
    OUTPUT:
        df: the same df with additional 'duplicatedStride' columns
    """
    df['duplicatedStride'] = df.widthMotion.duplicated(keep=False) & \
                             df.strideLength.duplicated(keep=False) & \
                             df.strideElevation.duplicated(keep=False)
                             
    return df


def strideRemover(df, listOfCondition):
    """ 
    This function remove the stride that do not fit the quality check requirements
    
    INPUT:
        df: a record imported as dataframe   
    OUTPUT:
        df: the same df with less strides
    """
    df = (df.loc[(~df[listOfCondition]).sum(axis=1) == 0]).copy()
    df = df.drop_duplicates(subset=['widthMotion', 'strideLength', 'strideElevation'], keep = False)
    # keep = False : Drop all duplicates.
    
    return df

    

def strideQualityCheckLevel1(df, startTime, unipedalMetricRange):
    """
    This function is the main of strideQualityCheck.py script.
    It perform the labelling for all quality check that will results in the removal of the stride
    
    INPUT:
        df: a record imported as dataframe 
        startTime: the startTime of the record
        stopTime: the stopTime of the record
        unipedalMetricRange: the range that metrics shoudl respect

    """
    df = strideInsideRecord(df, startTime)
    df = intraStrideSequenceConsistency(df)
    df = interStrideSequenceConsistency(df)
    df = nanCheck(df)
    df = duplicatedStrideCheck(df)
    df = labelOutlierMetric(df, unipedalMetricRange, mode='hard')
    
    return df
