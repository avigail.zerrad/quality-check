#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 09:51:25 2021

@author: cyrilb
"""


# Need to be rewrite with comprehension list (?)
def headerCheck(df, metricList):
    """
    Verifiy that the header of all columns are compliant with universal format
    
    INPUT: 
        df: a record imported as dataframe
        metricList: a list of the expected metric in the df
    OUPUT:
        True if all col are present
        a list of issue if it's not the case
    """
    issueList = []
    for col in df.columns:
        if col in metricList:
            pass
        else:
            issueList.append(col)
    
    return len(issueList)

def recordTimeConsistency(df, startTime, stopTime, clientTimestampRange, myDic , recordVersion=''):
    """ This function check the inclusion of startTime and stopTime in a given range of value
            lowerTimeBound < startTime < upperTimeBound
            lowerTimeBound < stopTime < upperTimeBound
        Input: 
            startTime of the record (int)
            stopTime of the record (int)
            timeRange: a dictionnary containing a list [lowerBound, upperBound] (2 int)
        Output:
            a dictionnary containing 4 keys ['staT_inf'], ['staT_sup'], ['stoT_inf'], ['staT_inf']
            with a Bool value (True: data is inside range)
            
    Note: currently the versionning is not implemented
    Note Cyril: the try catch is require but a prior ETL step could make it 'useless'
    """
    
    # Time consistency
    try:
        myDic['staT_inf'] = startTime > clientTimestampRange[0]
        myDic['staT_sup'] = startTime < clientTimestampRange[1]
        myDic['stoT_inf'] = stopTime > clientTimestampRange[0]
        myDic['stoT_sup'] = stopTime < clientTimestampRange[1]
    except: 
        print('error in ')
                
    return myDic

def recordStructureConsistency(df, metricList, myDic):
    """ 
    Function that check the number of column and the consistency of their name
    INPUT: 
        df: a record imported as dataframe
        metricList: A list containing the expected name of each metric 
        myDic: a dictionnary where computation will be returned
    OUTPUT:
        a dictionnary containing:
            'colNumber': the number of column in the df
            'headerIssue': the number of col inside the df that are not in the metricList
                
    DEPENDENCIES:
        this function require headerCheck()
        
    Note Cyril: not sure if the try catch is require
    Note Cyril: not sure the headerCheck work as expected
    """
    # Columns consistency
    try:
        myDic['colNumber'] = len(df.columns)
        myDic['headerIssue'] = headerCheck(df, metricList)  
    except:
        print('Error during recordQualityCheck')
    return myDic

# WIP
def recordQualityCheckLevel1(df, startTime, stopTime, clientTimestampRange, metricList):
    """ 
    MAIN function to perform some preliminary QualityCheck (ETL?) at record level
    
    INPUT: 
        df: a record imported as dataframe
        startTime of the record (int)
        stopTime of the record (int)
        clientTimestampRange: a list of 2 elements [lowerBound, upperBound] which are the Time Range
        metricList: A list containing the expected name of each metric 
        
    OUTPUT:
        a dictionnary hat contain:
            colNumber : the nb of columns in the df
            headerIssue: the number a column header not in the metricList
            4 keys ['staT_inf'], ['staT_sup'], ['stoT_inf'], ['staT_inf']
            with a Bool value (True: startTime (staT) or stopTime (stoT) inside limit)
            
    
    DEPENDENCIES:
        this function require recordTimeConsistency()
        this function require recordStructureConsistency()
    """
    # Instantiate a dic that will store some QC aggregated metrics
    myDic = {}
    myDic = recordTimeConsistency(df, startTime, stopTime, clientTimestampRange, myDic , recordVersion='')
    myDic = recordStructureConsistency(df, metricList, myDic)
    
    return myDic
    