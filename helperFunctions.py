#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 09:52:06 2021

@author: cyrilb
"""

def computeDistance(df, timeMetric = 'timeHeelStrike2', stopTime = ''):
    """ 
    Implementation of the algorithme use to compute Distance in Eval Mode
    
    Note: Eval Method use clientTimestamp and loose the last stride
    """

    # Instantiate variable for eval length stride calculation
    startTime = 0
    numberStrides = 0
    sumVelocity = 0
    fullDistance = 0
    previousStrideTime = 0
    distanceIntervalWithoutCurrentStride = 0
    distanceCurrentInterval = 0
    
    for i in df.index:
        clientTimestamp = df.loc[i, timeMetric]
        velocity = df.loc[i, 'velocity']
        
        if (numberStrides == 0):
            startTime = clientTimestamp
            previousStrideTime = clientTimestamp
            sumVelocity = 0
            distanceIntervalWithoutCurrentStride = 0
        
        endTime = clientTimestamp
        
        if ((endTime - previousStrideTime) > (10 * 1000)):
            fullDistance += distanceIntervalWithoutCurrentStride
            # if ther is more than 10 seconds, the current stride might not
            # event be correct, no need to keep it for the next interval.
            # We can reset everything
            startTime = 0
            sumVelocity = 0
            numberStrides = 0
            previousStrideTime = 0
            distanceIntervalWithoutCurrentStride = 0
        
        else:
            previousStrideTime = endTime
            sumVelocity += velocity
            numberStrides += 1
            distanceCurrentInterval = (sumVelocity / numberStrides) * ((endTime - startTime)) / 1000
            
        if (distanceCurrentInterval >= 5):
            # we keep the current interval
            fullDistance += distanceCurrentInterval
            # Instead of reseting all variables, we are re-using the
            # current stride as the first stride. If we're resetting
            # everything we would skip the distance between this stride
            # and the next one
            startTime = endTime
            sumVelocity = velocity
            previousStrideTime = startTime
            distanceIntervalWithoutCurrentStride = 0
            numberStrides = 1
            
        else:
            distanceIntervalWithoutCurrentStride = distanceCurrentInterval 
    

    if stopTime == '':
        lastStrideDistance = 0
    
    else: 
        lastStride = (stopTime - df.timeHeelStrike2.max())
        if lastStride > 0:
            lastStrideDistance = lastStride*df.loc[-4:, 'velocity'].mean()
    
    
    return fullDistance + distanceIntervalWithoutCurrentStride + lastStrideDistance

def dataEnhancementDeltaT(df):
    """ 
    This function compute metrics that involved DeltaT.
    These metrics will be require in the Quality Check process
    One exception: timestampToeOff which is not use
    
    INPUT:
        a record as dataframe
    
    OUTPUT:
        a  record as dataframe with aditional metrics
    """
    
    df['deltaT'] = df.clientTimestamp - df.timestamp
    df['timestampHeelStrike1'] = df.timeHeelStrike1 - df.deltaT
    df['timestampToeOff'] = df.timeToeOff - df.deltaT
    df['timestampHeelStrike2'] = df.timeHeelStrike2 - df.deltaT
    
    deltaT_Median = df.groupby(['side']).deltaT.median().astype(int)
    for a_side in df.side.unique():
        df.loc[df.side == a_side, 'deltaDeltaT'] = df.deltaT - deltaT_Median[a_side]
        
    return df


def labelOutlierMetric(df, metricRange, mode = 'hard'):
    """ 
    This function check if the data of a metric is inferior or superior to a range
    INPUT:
        df: a record imported as dataframe
        metricRange: a dictionnary that contain for each metric of interest a limit in f and a limit sup ex: {"velocity": [0, 100]}
        mode: a string that will be use to label the output
    OUTPUT:
        a df with additional columns labeled with the name METRIC_MODE_inf and METRIC_MODE_sup
        If the data is inside the range, it return True. False if the data is outside the range
    """
    for a_metric in metricRange.keys():
        df[a_metric+'_'+mode+'_inf'] = (df.loc[:,a_metric] > metricRange[a_metric][0]).copy()
        df[a_metric+'_'+mode+'_sup'] = (df.loc[:,a_metric] < metricRange[a_metric][1]).copy()
        
    return df 
