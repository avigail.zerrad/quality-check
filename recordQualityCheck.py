#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 09:49:50 2021

@author: cyrilb
"""

from helperFunctions import computeDistance


def numberOfStride(df, myDic):
    """ """
    myDic['leftStride'] = len(df.loc[df.side =='left'])
    myDic['rightStride'] = len(df.loc[df.side =='right'])
    myDic['totalStride'] = len(df)
    
    return myDic

def computeAsymmetryLR(df, myDic):
    """ """
    if max(myDic['leftStride'], myDic['rightStride']) == myDic['leftStride']:
        tmpLeft = myDic['leftStride'] -1
        tmpRight = myDic['rightStride']
    else:
        tmpLeft = myDic['leftStride']
        tmpRight =  myDic['rightStride'] -1
    
    if len(df) == 0 or tmpLeft == 0 or tmpRight == 0:
        myDic['asymmetryLR'] = -1
        
    elif myDic['leftStride'] == myDic['rightStride']: 
        myDic['asymmetryLR'] = round(myDic['leftStride']/myDic['totalStride'], 1)
        
    else:            
        myDic['asymmetryLR'] = round(tmpLeft/(tmpLeft + tmpRight), 2)
        
    return myDic

        
def computeAlternanceLR(df, myDic):
    """ """
    if max(myDic['leftStride'], myDic['rightStride']) == myDic['leftStride']:
        tmpLeft = myDic['leftStride'] -1
        tmpRight = myDic['rightStride']
    else:
        tmpLeft = myDic['leftStride']
        tmpRight =  myDic['rightStride'] -1
        
    if len(df) == 0 or tmpLeft == 0 or tmpRight == 0:
        myDic['alternanceLR'] = -1
    
    elif myDic['leftStride'] == myDic['rightStride']: 
        myDic['alternanceLR'] = round( ((df.side != df.side.shift()).sum() - 1)/ len(df), 1)
        
    else:            
        myDic['alternanceLR'] = round( ((df.side != df.side.shift()).sum() - 1)/ (tmpLeft + tmpRight), 2)
        
    return myDic


def checkInsoleAvailable(df, myDic):
    """ Count the number of insole with enough stride"""
    if (len(df) == 0):
        myDic['insoleAvailable'] = 0
        
    elif (len(df.loc[df.side == 'left']) == 0) or (len(df.loc[df.side == 'right']) == 0):
        myDic['insoleAvailable'] = 1
        
    else: 
        myDic['insoleAvailable'] = 2
        
    return myDic


def recordQualityCheckLevel1(df):
    """
    INPUT:
        df: a record imported as dataframe 

    """
    myDic = {}
    
    myDic = numberOfStride(df, myDic)
    myDic = computeAsymmetryLR(df, myDic)
    myDic = computeAlternanceLR(df, myDic)
    myDic['QC1distance'] = round(computeDistance(df),1)
    myDic = checkInsoleAvailable(df, myDic)
    
    return myDic


