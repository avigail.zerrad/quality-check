import pandas as pd


timeRange = {'clientTimestamp': [0, 4102441200000],
             'timestamp': [0, 4102441200000]
             }

# This is the list of 'metrics' present in CSV Version 6
# A versionning of this list will be require
metricList = ['side', 'timestamp', 'clientTimestamp'
              , 'timeHeelStrike1', 'timeHeelStrike2', 'timeToeOff'
              , 'strideLength', 'strideElevation', 'widthMotion'
              , 'strideDuration', 'swingDuration', 'stanceDuration'
              , 'swingPercentage', 'stancePercentage'
              , 'velocity', 'cadence'
              , 'doubleSupportPercentage', 'doubleSupportDuration'
              , 'singleSupportPercentage', 'singleSupportDuration'
              , 'stepTime', 'stepLength'
              , 'asymmetryParamStrideLength', 'asymmetryParamStrideDuration'
              , 'asymmetryParamStepLength', 'asymmetryParamStepTime', 'asymmetryParamSingleSupportPercentage'
              , 'capa0', 'capa1', 'capa2', 'capa3', 'capa4', 'capa5', 'capa6', 'capa7', 'capa8', 'capa9', 'capa10'
              , 'capa11', 'capa12', 'capa13', 'capa14', 'capa15', 'capa16', 'capa17'
              , 'cop0', 'cop1', 'cop2', 'cop3', 'cop4', 'cop5', 'cop6', 'cop7', 'cop8', 'cop9', 'cop10'
              , 'cop11', 'cop12', 'cop13', 'cop14', 'cop15'
              , 'overflow', 'epoch', 'flag']


official_range = {"velocity": [0, 5],
                "strideLength": [0, 2],
                "cadence": [0, 200],
                "swingDuration": [100, 1000],
                "stanceDuration": [100, 60000],
                "strideDuration": [0, 2000],
                "swingPercentage": [0, 70],
                "stancePercentage": [0, 100],
                "stepTime": [0, 800], 
                "asymmetryParamStepTime": [-1000, 2000],
                "singleSupportDuration": [0, 500],
                "doubleSupportDuration": [0, 600],
                "singleSupportPercentage": [0, 50],
                "doubleSupportPercentage": [0, 50],
                "asymmetryParamStrideDuration": [-2000, 4000],
                "asymmetryParamStrideLength" : [-2, 2],
                "asymmetryParamSingleSupportPercentage": [0, 80],
                "strideElevation": [-70, 70]
                }

unipedalMetricRange = {"velocity": [0, 5],
                "strideLength": [0, 3],
                "cadence": [0, 200],
                "swingDuration": [0, 60000],
                "stanceDuration": [0, 60000],
                "strideDuration": [0, 60000],
                "strideElevation": [-100, 100],
                "widthMotion": [-5, 5]
                }


metricRangeTest = {"velocity": [0, 5],
                "strideLength": [0, 3],
                "cadence": [0, 200],
                "swingDuration": [0, 60000],
                "stanceDuration": [0, 60000],
                "strideDuration": [0, 60000],
                "strideElevation": [-100, 100],
                "widthMotion": [-5, 5]
                }


                                
qualityCheckUnipedalMetricListHard = [str(el+side) for side in ['_hard_inf', '_hard_sup'] for el in unipedalMetricRange]


listOfCondition = ['cTs_staT', 'tHS1_staT',
       'tTO_staT', 'tHS2_staT', 'velocity_hard_inf', 'velocity_hard_sup',
       'strideLength_hard_inf', 'strideLength_hard_sup', 'cadence_hard_inf',
       'cadence_hard_sup', 'swingDuration_hard_inf', 'swingDuration_hard_sup',
       'stanceDuration_hard_inf', 'stanceDuration_hard_sup',
       'strideDuration_hard_inf', 'strideDuration_hard_sup',
       'strideElevation_hard_inf', 'strideElevation_hard_sup',
       'widthMotion_hard_inf', 'widthMotion_hard_sup', 'tHS1_tTO', 'tHS1_tHS2',
       'tTO_tTHS2', 'tHS1_cTs', 'tTO_cTs', 'tHS2_cTs', 'ts2_ts1', 'cTs2_cTs1']



qualityCheckMetric = [str(el+side) for side in ['_adapt_inf', '_adapt_sup'] for el in unipedalMetricRange]
qualitySequence = ['ts2_ts1', 'cTs2_cTs1']

#%%

def qualityCheck(df = '', startTime = '', recordType = ''):
    """
    This function try to load a record and if it succeed, perform a qualityCheck on it
    """
    l = len(df)

    # recordQualityCheck Level1
    dicQC = recordQualityCheckLevel1(df)
    
    
    # DATA PREPARATION
    df.sort_values('timeToeOff', inplace = True)
    df.reset_index(drop = True, inplace = True)
    #df = dataEnhancementDeltaT(df)


    # strideQualityCheck Level1
    # stride are removed if they do not fit the requirement.
    df = strideQualityCheckLevel1(df, startTime, unipedalMetricRange)
    
    # Extract in a df the Previous Quality Check 
    #dfQC1 = df.iloc[:,dicQC['colNumber']:]
    totalStrideNumber = len(df)
    totalColumnsNumberAfterQC1 = len(df.columns)
    
    # Remove Stride with problems
    df = strideRemover(df, listOfCondition) 

    
    # UNIPEDAL
    # The function to define metricRange is not implemented
    # 3 Mai 2021: this step is useless, metricRangeTest must be removed
    #df = unipedalMetricFiltering(df, metricRangeTest)

    
    #return df.iloc[:,:dicQC['colNumber']], dfQC1, df.iloc[:, totalColumnsNumberAfterQC1:], dicQC, dicAgg
    return dicQC, df.iloc[:, 0:l]


if __name__ == "__main__":
    
    strides =[]
    for file in files_testers : 
      startTime, df = read_df(file)
      record_QC, df =  qualityCheck(df, startTime)
      strides.append(df)

    dataset = pd.concat(strides)
    dataset.reset_index(drop=True)
