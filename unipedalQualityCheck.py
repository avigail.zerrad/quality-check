#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 09:49:14 2021

@author: cyrilb
"""

from helperFunctions import labelOutlierMetric

def swingDurationDepencies(df):
    """
    Definition: swingDuration = timeHeelStrike2 - timeToeOff
    """
    df.loc[~(df.stanceDuration_adapt_inf & df.stanceDuration_adapt_sup \
           & df.swingDuration_adapt_inf & df.swingDuration_adapt_sup \
           & df.strideDuration_adapt_inf & df.strideDuration_adapt_sup \
           & (df.swingDuration < df.strideDuration)), 'swingDuration'] = -666 
    return df
    
def stanceDurationDependencies(df):
    """ 
    Definition: stanceDuration = timeToeOff - timeHeelStrike1
    """
    df.loc[~(df.stanceDuration_adapt_inf & df.stanceDuration_adapt_sup \
             & df.swingDuration_adapt_inf & df.swingDuration_adapt_sup \
             & df.strideDuration_adapt_inf & df.strideDuration_adapt_sup \
             & (df.stanceDuration < df.strideDuration)), 'stanceDuration'] = -666
    return df

def strideDurationDependencies(df):
    """ 
    Definition: strideDuration = timeHeelStrike2 - timeHeelstrike1
    """
    df.loc[~(df.strideDuration_adapt_inf & df.strideDuration_adapt_inf), 'strideDuration'] = -666
    return df

# MISTAKE strideElevation can be = -1
def strideElevationDependencies(df):
    """ 
    Definition: computed inside the insole
    """
    df.loc[~(df.strideElevation_adapt_inf & df.strideElevation_adapt_sup), 'strideElevation'] = -666  
    return df

def strideLengthDependencies(df):
    """ 
    Definition: computed inside the insole
    """  
    df.loc[~(df.strideLength_adapt_inf & df.strideLength_adapt_sup), 'strideLength'] = -666
    return df

# MISTAKE widthMotion can be = -1
def widthMotionDependencies(df):
    """ 
    Definition: computed inside the insole
    """
    df.loc[~(df.widthMotion_adapt_inf & df.widthMotion_adapt_sup), 'widthMotion'] = -666 
    return df

def swingPercentageDependencies(df):
    """ 
    Definition: swingPercentage = round((swingDuration / strideDuration) * 100)
    
    Note: No range check for this metric: I consider that swingPercentage can't have aberrant value as soon as
    stanceDuration, swingDuration and strideDuration are validated"""
    df.loc[~(df.stanceDuration_adapt_inf & df.stanceDuration_adapt_sup \
           & df.swingDuration_adapt_inf & df.swingDuration_adapt_sup \
           & df.strideDuration_adapt_inf & df.strideDuration_adapt_sup), 'swingPercentage'] = -666 
    return df

def stancePercentageDependencies(df):
    """ 
    Definition : stancePercentage = 100 - swingPercentage
    
    Note: No range check for this metric: I consider that stancePercentage can't have aberrant value as soon as
    stanceDuration, swingDuration and strideDuration are validated
    """
    df.loc[~(df.stanceDuration_adapt_inf & df.stanceDuration_adapt_sup \
           & df.swingDuration_adapt_inf & df.swingDuration_adapt_sup \
           & df.strideDuration_adapt_inf & df.strideDuration_adapt_sup), 'stancePercentage'] = -666 
    return df
    
def velocityDependecies(df):
    """
    Definition: velocity = strideLength / (strideDuration / 1000)
    """
    df.loc[~(df.strideDuration_adapt_inf  & df.strideDuration_adapt_sup \
         & df.strideLength_adapt_inf & df.strideLength_adapt_sup \
         & df.velocity_adapt_inf & df.velocity_adapt_sup), 'velocity'] = -666
    return df

def cadenceDependencies(df):
    """
    Definition: cadence = round(60 / (strideDuration / 1000)) * 2
    """
    df.loc[~(df.strideDuration_adapt_inf & df.strideDuration_adapt_sup & df.cadence), 'cadence'] = -666
    return df
    
def unipedalMetricFiltering(df, metricRangeTest):
    """ 
    WARNING: the metricRangeTest have to be replace by the function that will determine
    automatic adaptive Range for each record
    Function that set to -1 Metric that require dependency with other metrics to be valid
    
    INPUT:
        the df output of labelOutlierMetric(mode='adapt')
        
    OUTPUT:
        a dataframe
    """
    
    # The labelling of metric outlier require metricRangeTest
    # The function to determine metricRangeTest is not yet implemented
    df = labelOutlierMetric(df, metricRangeTest, mode='adapt')
    
    df = swingDurationDepencies(df)
    df = stanceDurationDependencies(df)
    df = strideDurationDependencies(df)
    df = strideElevationDependencies(df) # 210426 Function is wrong
    df = strideLengthDependencies(df)
    df = widthMotionDependencies(df) # 210426 Function is wrong
    df = swingPercentageDependencies(df)
    df = stancePercentageDependencies(df)
    df = velocityDependecies(df)
    df = cadenceDependencies(df)

    return df